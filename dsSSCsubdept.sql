--tab=SSCSubDept
with current_date as (
     select 
     to_number (cc.calendaryear) as year
     , min(cc.tradingdate) as start_date
     , max(cc.tradingdate) as end_date
     , to_number(min(to_number(cc.calendarweek))) as start_week 
     , to_number(max(to_number(cc.calendarweek))) as end_week
     from custom.calendar cc
     where cc.calendaryear = to_number(decode(:Year_P, 0, (select cc.calendaryear from custom.calendar cc where tradingdate = trunc(sysdate-1)), :Year_P))
     and cc.calendarweek 
         between to_number(decode(:StartWeek_P, 0, (select cc.calendarweek from custom.calendar cc where tradingdate = trunc(sysdate-1)), :StartWeek_P)) 
         and to_number(decode(:EndWeek_P, 0, (select cc.calendarweek from custom.calendar cc where tradingdate = trunc(sysdate-1)), :EndWeek_P))
     group by cc.calendaryear
),

Weeks as (
      select d.*
      , (select c.calendarweek from custom.calendar c where c.tradingdate = d.LY_startdate) as LY_startweek
      , (select c.calendarweek from custom.calendar c where c.tradingdate = d.LY_enddate) as LY_endweek
      from 
      (select cd.year CY_year
      , cd.start_week CY_startweek
      , cd.end_week  CY_endweek 
      , min(ccly.calendaryear) as LY_startyear
      , max(ccly.calendaryear) as LY_endyear
      , min(ccly.tradingdate) as LY_startdate
      , max(ccly.tradingdate) as LY_enddate
      from current_date cd
      inner join custom.calendar ccly on ccly.tradingdate between cd.start_date-364 and cd.end_date-364
      group by cd.year
            , cd.start_week
            , cd.end_week) d
)

SELECT EmpName
  , DescriptionCol1
  , DescriptionCol2
  , DescriptionCol3
  , DescriptionCol4
  , DescriptionCol5
  , DescriptionCol6
  
  -- LY
  , Col1SalesLY
  , Col1UnitsLY
  , Col1RegSalesLY
  , CASE WHEN Col1SalesLY = 0 THEN 0 ELSE Round(100 * (Col1RegSalesLY / Col1SalesLY), 0) END As Col1RegularSalesPctLY
  
  , Col2SalesLY
  , Col2UnitsLY
  , Col2RegSalesLY
  , CASE WHEN Col2SalesLY = 0 THEN 0 ELSE Round(100 * (Col2RegSalesLY / Col2SalesLY), 0) END As Col2RegularSalesPctLY

  , Col3SalesLY
  , Col3UnitsLY
  , Col3RegSalesLY
  , CASE WHEN Col3SalesLY = 0 THEN 0 ELSE Round(100 * (Col3RegSalesLY / Col3SalesLY), 0) END As Col3RegularSalesPctLY

  , Col4SalesLY
  , Col4UnitsLY
  , Col4RegSalesLY
  , CASE WHEN Col4SalesLY = 0 THEN 0 ELSE Round(100 * (Col4RegSalesLY / Col4SalesLY), 0) END As Col4RegularSalesPctLY

  , Col5SalesLY
  , Col5UnitsLY
  , Col5RegSalesLY
  , CASE WHEN Col5SalesLY = 0 THEN 0 ELSE Round(100 * (Col5RegSalesLY / Col5SalesLY), 0) END As Col5RegularSalesPctLY

  , Col6SalesLY
  , Col6UnitsLY
  , Col6RegSalesLY
  , CASE WHEN Col6SalesLY = 0 THEN 0 ELSE Round(100 * (Col6RegSalesLY / Col6SalesLY), 0) END As Col6RegularSalesPctLY

  , OtherSalesLY
  , OtherUnitsLY
  , OtherRegSalesLY
  , CASE WHEN OtherSalesLY = 0 THEN 0 ELSE Round(100 * (OtherRegSalesLY / OtherSalesLY), 0) END As OtherRegularSalesPctLY
  
  , TotalSalesLY
  , TotalUnitsLY
  , TotalRegSalesLY
  , CASE WHEN TotalSalesLY = 0 THEN 0 ELSE Round(100 * (TotalRegSalesLY / TotalSalesLY), 0) END As TotalRegularSalesPctLY

  -- CY
  , Col1SalesCY
  , Col1UnitsCY
  , Col1RegSalesCY
  , CASE WHEN Col1SalesCY = 0 THEN 0 ELSE Round(100 * (Col1RegSalesCY / Col1SalesCY), 0) END As Col1RegularSalesPctCY
  
  , Col2SalesCY
  , Col2UnitsCY
  , Col2RegSalesCY
  , CASE WHEN Col2SalesCY = 0 THEN 0 ELSE Round(100 * (Col2RegSalesCY / Col2SalesCY), 0) END As Col2RegularSalesPctCY
    
  , Col3SalesCY
  , Col3UnitsCY
  , Col3RegSalesCY
  , CASE WHEN Col3SalesCY = 0 THEN 0 ELSE Round(100 * (Col3RegSalesCY / Col3SalesCY), 0) END As Col3RegularSalesPctCY
  
  , Col4SalesCY
  , Col4UnitsCY
  , Col4RegSalesCY
  , CASE WHEN Col4SalesCY = 0 THEN 0 ELSE Round(100 * (Col4RegSalesCY / Col4SalesCY), 0) END As Col4RegularSalesPctCY

  , Col5SalesCY
  , Col5UnitsCY
  , Col5RegSalesCY
  , CASE WHEN Col5SalesCY = 0 THEN 0 ELSE Round(100 * (Col5RegSalesCY / Col5SalesCY), 0) END As Col5RegularSalesPctCY

  , Col6SalesCY
  , Col6UnitsCY
  , Col6RegSalesCY
  , CASE WHEN Col6SalesCY = 0 THEN 0 ELSE Round(100 * (Col6RegSalesCY / Col6SalesCY), 0) END As Col6RegularSalesPctCY

  , OtherSalesCY
  , OtherUnitsCY
  , OtherRegSalesCY
  , CASE WHEN OtherSalesCY = 0 THEN 0 ELSE Round(100 * (OtherRegSalesCY / OtherSalesCY), 0) END As OtherRegularSalesPctCY

  , TotalSalesCY
  , TotalUnitsCY
  , TotalRegSalesCY
  , CASE WHEN TotalSalesCY = 0 THEN 0 ELSE Round(100 * (TotalRegSalesCY / TotalSalesCY), 0) END As TotalRegularSalesPctCY

  , weekinfo.Year As Year_P
  , weekinfo.StartWeek As StartWeek_P
  , weekinfo.EndWeek As EndWeek_P

FROM (
  SELECT EmpName
    , DescriptionCol1
    , DescriptionCol2
    , DescriptionCol3
    , DescriptionCol4
    , DescriptionCol5
    , DescriptionCol6

    -- LY
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 1 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 1 THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 1 THEN Nvl(Sales, 0) ELSE 0 END) END) As Col1SalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 1 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 1 THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 1 THEN Nvl(Units, 0) ELSE 0 END) END) As Col1UnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 1 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 1 THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 1 THEN Nvl(RegularSales, 0) ELSE 0 END) END) As Col1RegSalesLY
  
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 2 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 2 THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 2 THEN Nvl(Sales, 0) ELSE 0 END) END) As Col2SalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 2 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 2 THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 2 THEN Nvl(Units, 0) ELSE 0 END) END) As Col2UnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 2 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 2 THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 2 THEN Nvl(RegularSales, 0) ELSE 0 END) END) As Col2RegSalesLY

    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 3 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 3 THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 3 THEN Nvl(Sales, 0) ELSE 0 END) END) As Col3SalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 3 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 3 THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 3 THEN Nvl(Units, 0) ELSE 0 END) END) As Col3UnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 3 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 3 THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 3 THEN Nvl(RegularSales, 0) ELSE 0 END) END) As Col3RegSalesLY

    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 4 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 4 THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 4 THEN Nvl(Sales, 0) ELSE 0 END) END) As Col4SalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 4 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 4 THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 4 THEN Nvl(Units, 0) ELSE 0 END) END) As Col4UnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 4 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 4 THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 4 THEN Nvl(RegularSales, 0) ELSE 0 END) END) As Col4RegSalesLY

    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 5 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 5 THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 5 THEN Nvl(Sales, 0) ELSE 0 END) END) As Col5SalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 5 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 5 THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 5 THEN Nvl(Units, 0) ELSE 0 END) END) As Col5UnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 5 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 5 THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 5 THEN Nvl(RegularSales, 0) ELSE 0 END) END) As Col5RegSalesLY

    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 6 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 6 THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 6 THEN Nvl(Sales, 0) ELSE 0 END) END) As Col6SalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 6 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 6 THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 6 THEN Nvl(Units, 0) ELSE 0 END) END) As Col6UnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 6 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 6 THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 6 THEN Nvl(RegularSales, 0) ELSE 0 END) END) As Col6RegSalesLY

    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 7 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 7 THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 7 THEN Nvl(Sales, 0) ELSE 0 END) END) As OtherSalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 7 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 7 THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 7 THEN Nvl(Units, 0) ELSE 0 END) END) As OtherUnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 and Col = 7 or numyear = LY_endyear and numweek between 1 and LY_endweek and Col = 7 THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek and Col = 7 THEN Nvl(RegularSales, 0) ELSE 0 END) END) As OtherRegSalesLY 

    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 or numyear = LY_endyear and numweek between 1 and LY_endweek THEN Nvl(Sales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek THEN Nvl(Sales, 0) ELSE 0 END) END) As TotalSalesLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 or numyear = LY_endyear and numweek between 1 and LY_endweek THEN Nvl(Units, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek THEN Nvl(Units, 0) ELSE 0 END) END) As TotalUnitsLY 
    , Sum(CASE WHEN LY_startyear != LY_endyear 
         THEN (CASE WHEN numyear = LY_startyear and numweek between LY_startweek and 53 or numyear = LY_endyear and numweek between 1 and LY_endweek THEN Nvl(RegularSales, 0) ELSE 0 END)   
         ELSE (Case WHEN numyear between LY_startyear and LY_endyear and numweek between LY_startweek and LY_endweek THEN Nvl(RegularSales, 0) ELSE 0 END) END) As TotalRegSalesLY 


    -- CY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 1 THEN Nvl(Sales, 0) ELSE 0 END) As Col1SalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 1 THEN Nvl(Units, 0) ELSE 0 END) As Col1UnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 1 THEN Nvl(RegularSales, 0) ELSE 0 END) As Col1RegSalesCY
  
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 2 THEN Nvl(Sales, 0) ELSE 0 END) As Col2SalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 2 THEN Nvl(Units, 0) ELSE 0 END) As Col2UnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 2 THEN Nvl(RegularSales, 0) ELSE 0 END) As Col2RegSalesCY
    
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 3 THEN Nvl(Sales, 0) ELSE 0 END) As Col3SalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 3 THEN Nvl(Units, 0) ELSE 0 END) As Col3UnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 3 THEN Nvl(RegularSales, 0) ELSE 0 END) As Col3RegSalesCY
  
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 4 THEN Nvl(Sales, 0) ELSE 0 END) As Col4SalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 4 THEN Nvl(Units, 0) ELSE 0 END) As Col4UnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 4 THEN Nvl(RegularSales, 0) ELSE 0 END) As Col4RegSalesCY

    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 5 THEN Nvl(Sales, 0) ELSE 0 END) As Col5SalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 5 THEN Nvl(Units, 0) ELSE 0 END) As Col5UnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 5 THEN Nvl(RegularSales, 0) ELSE 0 END) As Col5RegSalesCY

    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 6 THEN Nvl(Sales, 0) ELSE 0 END) As Col6SalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 6 THEN Nvl(Units, 0) ELSE 0 END) As Col6UnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 6 THEN Nvl(RegularSales, 0) ELSE 0 END) As Col6RegSalesCY 

    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 7 THEN Nvl(Sales, 0) ELSE 0 END) As OtherSalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 7 THEN Nvl(Units, 0) ELSE 0 END) As OtherUnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek and Col = 7 THEN Nvl(RegularSales, 0) ELSE 0 END) As OtherRegSalesCY        

    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek THEN Nvl(Sales, 0) ELSE 0 END) As TotalSalesCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek THEN Nvl(Units, 0) ELSE 0 END) As TotalUnitsCY
    , Sum(CASE WHEN numyear = CY_year and numweek between CY_startweek and CY_endweek THEN Nvl(RegularSales, 0) ELSE 0 END) As TotalRegSalesCY


  FROM (
    SELECT EmpName
      , numyear
      , numweek
      , YearDate
      , CY_year
      , CY_startweek
      , CY_endweek
      , LY_startyear
      , LY_endyear
      , LY_startweek
      , LY_endweek      
      , Col
      , DescriptionCol1
      , DescriptionCol2
      , DescriptionCol3
      , DescriptionCol4
      , DescriptionCol5
      , DescriptionCol6
      , Sum(Units) As Units
      , Sum(Sales) As Sales
      , Sum(RegularSales) As RegularSales
    FROM (
      WITH subdept As 
      (
        SELECT 1 As Col, Subdept_Col_1 As SubDept
          , (SELECT Description FROM vstore.Merchandise WHERE pkMerchandiseNo LIKE '%' || Subdept_Col_1 AND fkMdseLevelNo = 3 AND fkorganizationno = :BusinessUnit_P) As Descr 
            -- FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = '0' || :Stores_P
            FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = :Stores_P
          UNION
        SELECT 2 As Col, Subdept_Col_2 As SubDept
          , (SELECT Description FROM vstore.Merchandise WHERE pkMerchandiseNo LIKE '%' || Subdept_Col_2 AND fkMdseLevelNo = 3 AND fkorganizationno = :BusinessUnit_P) As Descr 
            -- FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = '0' || :Stores_P
            FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = :Stores_P
          UNION
        SELECT 3 As Col, Subdept_Col_3 As SubDept
          , (SELECT Description FROM vstore.Merchandise WHERE pkMerchandiseNo LIKE '%' || Subdept_Col_3 AND fkMdseLevelNo = 3 AND fkorganizationno = :BusinessUnit_P) As Descr 
            -- FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = '0' || :Stores_P
            FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = :Stores_P
          UNION
        SELECT 4 As Col, Subdept_Col_4 As SubDept
          , (SELECT Description FROM vstore.Merchandise WHERE pkMerchandiseNo LIKE '%' || Subdept_Col_4 AND fkMdseLevelNo = 3 AND fkorganizationno = :BusinessUnit_P) As Descr 
             --FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = '0' || :Stores_P
             FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = :Stores_P
          UNION
        SELECT 5 As Col, Subdept_Col_5 As SubDept
          , (SELECT Description FROM vstore.Merchandise WHERE pkMerchandiseNo LIKE '%' || Subdept_Col_5 AND fkMdseLevelNo = 3 AND fkorganizationno = :BusinessUnit_P) As Descr 
             --FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = '0' || :Stores_P
             FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = :Stores_P
          UNION
        SELECT 6 As Col, Subdept_Col_6 As SubDept
          , (SELECT Description FROM vstore.Merchandise WHERE pkMerchandiseNo LIKE '%' || Subdept_Col_6 AND fkMdseLevelNo = 3 AND fkorganizationno = :BusinessUnit_P) As Descr 
             --FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = '0' || :Stores_P
             FROM Payroll.ScoreCard_Prefs_Subdept WHERE StoreCode = :Stores_P
      )

      SELECT (emp.LName || ', ' || emp.FName) As EmpName
        , (CASE WHEN ehl.YearDate = w.CY_Year THEN 'CY' WHEN ehl.YearDate = w.LY_startyear THEN 'LY' END) As YearDate
        , ehl.Yeardate as numyear
        , ehl.Weekdate as numweek
        , w.CY_year
        , w.CY_startweek
        , w.CY_endweek
        , w.LY_startyear
        , w.LY_endyear
        , w.LY_startweek
        , w.LY_endweek
        , Nvl(subdept.Col, 7) As Col
        , subdept.Descr As Description
        , (SELECT Descr FROM subdept WHERE Col = 1) As DescriptionCol1
        , (SELECT Descr FROM subdept WHERE Col = 2) As DescriptionCol2
        , (SELECT Descr FROM subdept WHERE Col = 3) As DescriptionCol3
        , (SELECT Descr FROM subdept WHERE Col = 4) As DescriptionCol4
        , (SELECT Descr FROM subdept WHERE Col = 5) As DescriptionCol5
        , (SELECT Descr FROM subdept WHERE Col = 6) As DescriptionCol6
        , ehl.Sales
        , ehl.Num_Units As Units
        , ehl.Regular_Sales As RegularSales

      FROM Payroll.Emp emp
        INNER JOIN Payroll.Emp_MHL ehl ON emp.Emp_Num = ehl.Emp_Num
        --LEFT OUTER JOIN subdept ON LPad(ehl.Subdept, 4, '0') = subdept.SubDept
       LEFT OUTER JOIN subdept ON LPad(ehl.Subdept, 4, '0') = lpad(subdept.SubDept, 4,'0') --Added by DBA
        inner join weeks w on 1=1
      WHERE emp.Prim_Store_Cd = :Stores_P

        AND Trim(emp.emp_num) = 
          CASE WHEN :ManagerNum_P IS Null
            THEN Trim(emp.Emp_Num)
          ELSE Nvl((
              SELECT Trim(Max(emg.Emp_Num))
              FROM payroll.Emp_Manager emg
              WHERE Trim(emg.Emp_Num) = Trim(emp.Emp_Num)
                AND LPad(Trim(emg.Manager_Num), 5, '0') = LPad(Trim(:ManagerNum_P), 5, '0')
            ),'00000')
          END
        AND ((LY_startyear != LY_endyear
        AND ehl.YearDate = w.LY_startyear and ehl.WeekDate between w.LY_startweek and 53
        OR ehl.YearDate = w.LY_endyear and ehl.WeekDate between 1 and w.LY_endweek
        OR ehl.YearDate = w.CY_year and ehl.WeekDate between w.CY_startweek and w.CY_endweek)
           OR
        (LY_startyear = LY_endyear
        AND ehl.YearDate = w.LY_startyear and ehl.WeekDate between w.LY_startweek and w.LY_endweek
        OR ehl.YearDate = w.CY_year and ehl.WeekDate between w.CY_startweek and w.CY_endweek))  

        AND emp.Status IN ('A', 'I') 
        AND emp.Assoc_Tp IN ('M', 'S')
      ORDER BY emp.LName || ', ' || SubStr(emp.FName, 1, 1), Col
    )
    GROUP BY EmpName
      , numyear
      , numweek
      , YearDate
      , CY_year
      , CY_startweek
      , CY_endweek
      , LY_startyear
      , LY_endyear
      , LY_startweek
      , LY_endweek          
      , YearDate
      , Col
      , DescriptionCol1
      , DescriptionCol2
      , DescriptionCol3
      , DescriptionCol4
      , DescriptionCol5
      , DescriptionCol6
    ORDER BY EmpName, YearDate DESC, Col
  )
  GROUP BY EmpName
    , DescriptionCol1
    , DescriptionCol2
    , DescriptionCol3
    , DescriptionCol4
    , DescriptionCol5
    , DescriptionCol6

  ORDER BY EmpName
) INNER JOIN (  
        SELECT CY_year AS year, CY_startweek AS startweek, CY_endweek as endweek
        FROM weeks w 
  ) weekInfo ON 1 = 1;