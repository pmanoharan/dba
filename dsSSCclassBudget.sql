--tab=ClassBudget
with current_date as (
     select 
     to_number (cc.calendaryear) as year
     , min(cc.tradingdate) as start_date
     , max(cc.tradingdate) as end_date
     , to_number(min(to_number(cc.calendarweek))) as start_week 
     , to_number(max(to_number(cc.calendarweek))) as end_week
     from custom.calendar cc
     where cc.calendaryear = to_number(decode(:Year_P, 0, (select cc.calendaryear from custom.calendar cc where tradingdate = trunc(sysdate-1)), :Year_P))
     and cc.calendarweek 
         between to_number(decode(:StartWeek_P, 0, (select cc.calendarweek from custom.calendar cc where tradingdate = trunc(sysdate-1)), :StartWeek_P)) 
         and to_number(decode(:EndWeek_P, 0, (select cc.calendarweek from custom.calendar cc where tradingdate = trunc(sysdate-1)), :EndWeek_P))
     group by cc.calendaryear
),

Weeks as (
      select d.*
      , (select c.calendarweek from custom.calendar c where c.tradingdate = d.LY_startdate) as LY_startweek
      , (select c.calendarweek from custom.calendar c where c.tradingdate = d.LY_enddate) as LY_endweek
      from 
      (select cd.year CY_year
      , cd.start_week CY_startweek
      , cd.end_week  CY_endweek 
      , min(ccly.calendaryear) as LY_startyear
      , max(ccly.calendaryear) as LY_endyear
      , min(ccly.tradingdate) as LY_startdate
      , max(ccly.tradingdate) as LY_enddate
      from current_date cd
      inner join custom.calendar ccly on ccly.tradingdate between cd.start_date-364 and cd.end_date-364
      group by cd.year
            , cd.start_week
            , cd.end_week) d
) 

SELECT Distinct Nvl(ColName1, 'N/A') As BUColName1
  , Nvl(ColName2, 'N/A') As BUColName2
  , Nvl(ColName3, 'N/A') As BUColName3
  , Nvl(ColName4, 'N/A') As BUColName4
  , Nvl(ColName5, 'N/A') As BUColName5
  , Nvl(ColName6, 'N/A') As BUColName6
  , Nvl(ColName7, 'N/A') As BUColName7

  , SalesLY1 As BUSalesLY1
  , SalesLY2 As BUSalesLY2
  , SalesLY3 As BUSalesLY3
  , SalesLY4 As BUSalesLY4
  , SalesLY5 As BUSalesLY5
  , SalesLY6 As BUSalesLY6
  , SalesLY7 As BUSalesLY7
  , (SalesLY1 + SalesLY2 + SalesLY3 + SalesLY4 + SalesLY5 + SalesLY6 + SalesLY7) As BUTotalLY
  
  , SalesCY1 As BUSalesCY1
  , SalesCY2 As BUSalesCY2
  , SalesCY3 As BUSalesCY3
  , SalesCY4 As BUSalesCY4
  , SalesCY5 As BUSalesCY5
  , SalesCY6 As BUSalesCY6
  , SalesCY7 As BUSalesCY7
  , (SalesCY1 + SalesCY2 + SalesCY3 + SalesCY4 + SalesCY5 + SalesCY6 + SalesCY7) As BUTotalCY

  , BudgetCY1
  , BudgetCY2
  , BudgetCY3
  , BudgetCY4
  , BudgetCY5
  , BudgetCY6
  , BudgetCY7
  , (BudgetCY1 + BudgetCY2 + BudgetCY3 + BudgetCY4 + BudgetCY5 + BudgetCY6 + BudgetCY7) As TotalBU

  , weekinfo.Year As Year_P
  , weekinfo.StartWeek As StartWeek_P
  , weekinfo.EndWeek As EndWeek_P

FROM (
  WITH salesLY As (
    SELECT YearDate, Col, Sum(SumLine) As Sales
    FROM (
      SELECT bgs.YearDate
        , CASE WHEN w.LY_startyear != w.LY_endyear 
         THEN (CASE WHEN bgs.yeardate = w.LY_startyear and bgs.calendarweek between w.LY_startweek and 53 
           or bgs.yeardate = w.LY_endyear and bgs.calendarweek between 1 and w.LY_endweek THEN Nvl(bgs.SumLine, 0) ELSE 0 END)   
         ELSE (Case WHEN bgs.yeardate between w.LY_startyear and w.LY_endyear and bgs.calendarweek between w.LY_startweek and w.LY_endweek THEN Nvl(bgs.SumLine, 0) ELSE 0 END) END as SumLine
              
        , CASE 
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_1) THEN 1
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_2) THEN 2
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_3) THEN 3
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_4) THEN 4
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_5) THEN 5
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_6) THEN 6
            ELSE 7
          END As Col

      FROM custom.BudgetSales bgs 
        --LEFT JOIN Payroll.Scorecard_Prefs scp ON bgs.StoreCode = scp.StoreCode
		LEFT JOIN Payroll.Scorecard_Prefs scp ON lpad(bgs.StoreCode,5,'0') = lpad(scp.StoreCode,5,'0') -- Added by DBA
        inner join weeks w on 1=1
      --WHERE bgs.StoreCode = LPad(:Stores_P, 5, '0')
	  WHERE lpad(bgs.StoreCode,5,'0') = LPad(:Stores_P, 5, '0') -- Added by DBA
         AND ((LY_startyear != LY_endyear
             AND bgs.YearDate = w.LY_startyear and bgs.calendarweek between w.LY_startweek and 53
             OR bgs.YearDate = w.LY_endyear and  bgs.calendarweek between 1 and w.LY_endweek
             OR bgs.YearDate = w.CY_year and  bgs.calendarweek between w.CY_startweek and w.CY_endweek)
                OR
             (LY_startyear = LY_endyear
             AND bgs.YearDate = w.LY_startyear and  bgs.calendarweek between w.LY_startweek and w.LY_endweek
             OR bgs.YearDate = w.CY_year and  bgs.calendarweek between w.CY_startweek and w.CY_endweek))
             )
    GROUP BY Col, YearDate
    ORDER BY Col, YearDate
  ), -- end salesLY

   salesCY As (
    SELECT YearDate, Col, Sum(SumLine) As Sales, Sum(BudgetAmount) As Budget
    FROM (
      SELECT bgs.YearDate
        , bgs.SumLine
        , bgs.BudgetAmount

        , CASE 
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_1) THEN 1
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_2) THEN 2
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_3) THEN 3
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_4) THEN 4
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_5) THEN 5
            WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_6) THEN 6
            ELSE 7
          END As Col

      FROM custom.BudgetSales bgs 
       -- LEFT JOIN Payroll.Scorecard_Prefs scp ON bgs.StoreCode = scp.StoreCode
	   LEFT JOIN Payroll.Scorecard_Prefs scp ON lpad(bgs.StoreCode,5,'0') = lpad(scp.StoreCode,5,'0') -- Added by DBA
        inner join weeks w on 1=1
      --WHERE bgs.StoreCode = LPad(:Stores_P, 5, '0')
	  WHERE lpad(bgs.StoreCode,5,'0') = LPad(:Stores_P, 5, '0') -- Added by DBA
        AND bgs.YearDate = w.CY_year
        AND bgs.Calendarweek between w.CY_startweek and w.Cy_endweek

    )

   GROUP BY Col, YearDate
    ORDER BY Col, YearDate
  ), -- end salesCY

  cls As (
    SELECT Class, Col
    FROM (
      SELECT CASE
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_1) THEN 
          (SELECT Max(Distinct Description) FROM vstore.Merchandise WHERE fkMdseLevelNo = 4 AND To_Number(SubStr(pkMerchandiseNo, 15, 2)) = To_Number(scp.Class_Col_1) and fkorganizationno = :BusinessUnit_P)
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_2) THEN 
          (SELECT Max(Distinct Description) FROM vstore.Merchandise WHERE fkMdseLevelNo = 4 AND To_Number(SubStr(pkMerchandiseNo, 15, 2)) = To_Number(scp.Class_Col_2) and fkorganizationno = :BusinessUnit_P)
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_3) THEN 
          (SELECT Max(Distinct Description) FROM vstore.Merchandise WHERE fkMdseLevelNo = 4 AND To_Number(SubStr(pkMerchandiseNo, 15, 2)) = To_Number(scp.Class_Col_3) and fkorganizationno = :BusinessUnit_P)
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_4) THEN 
          (SELECT Max(Distinct Description) FROM vstore.Merchandise WHERE fkMdseLevelNo = 4 AND To_Number(SubStr(pkMerchandiseNo, 15, 2)) = To_Number(scp.Class_Col_4) and fkorganizationno = :BusinessUnit_P)
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_5) THEN 
          (SELECT Max(Distinct Description) FROM vstore.Merchandise WHERE fkMdseLevelNo = 4 AND To_Number(SubStr(pkMerchandiseNo, 15, 2)) = To_Number(scp.Class_Col_5) and fkorganizationno = :BusinessUnit_P)
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_6) THEN 
          (SELECT Max(Distinct Description) FROM vstore.Merchandise WHERE fkMdseLevelNo = 4 AND To_Number(SubStr(pkMerchandiseNo, 15, 2)) = To_Number(scp.Class_Col_6) and fkorganizationno = :BusinessUnit_P)
        ELSE 'OTHERS'
      END As Class

/*
        SELECT CASE
          WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_1) THEN 
            (SELECT Vendor FROM custom.Vendor WHERE VendorID = To_Number(scp.Class_Col_1) AND fkOrganizationNo = :BusinessUnit_P)
          WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_2) THEN 
            (SELECT Vendor FROM custom.Vendor WHERE VendorID = To_Number(scp.Class_Col_2) AND fkOrganizationNo = :BusinessUnit_P)
          WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_3) THEN 
            (SELECT Vendor FROM custom.Vendor WHERE VendorID = To_Number(scp.Class_Col_3) AND fkOrganizationNo = :BusinessUnit_P)
          WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_4) THEN 
            (SELECT Vendor FROM custom.Vendor WHERE VendorID = To_Number(scp.Class_Col_4) AND fkOrganizationNo = :BusinessUnit_P)
          WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_5) THEN 
            (SELECT Vendor FROM custom.Vendor WHERE VendorID = To_Number(scp.Class_Col_5) AND fkOrganizationNo = :BusinessUnit_P)
          WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_6) THEN 
            (SELECT Vendor FROM custom.Vendor WHERE VendorID = To_Number(scp.Class_Col_6) AND fkOrganizationNo = :BusinessUnit_P)
          ELSE 'OTHERS'
        END As Class
*/
        
      , CASE 
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_1) THEN 1
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_2) THEN 2
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_3) THEN 3
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_4) THEN 4
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_5) THEN 5
        WHEN To_Number(SubStr(bgs.Class, 1, 2)) = To_Number(scp.Class_Col_6) THEN 6
        ELSE 7
      END As Col

      FROM custom.BudgetSales bgs 
        --LEFT JOIN Payroll.Scorecard_Prefs scp ON bgs.StoreCode = scp.StoreCode
		LEFT JOIN Payroll.Scorecard_Prefs scp ON lpad(bgs.StoreCode,5,'0') = lpad(scp.StoreCode,5,'0') -- Added by DBA
        inner join weeks w on 1=1
      --WHERE bgs.StoreCode = LPad(:Stores_P, 5, '0')
	  WHERE lpad(bgs.StoreCode,5,'0') = LPad(:Stores_P, 5, '0') -- Added by DBA
      AND bgs.YearDate = w.CY_year
      AND bgs.CalendarWeek between w.CY_startweek and w.CY_endweek
    )

    GROUP BY Class, Col
    ORDER BY Col
  ) -- end cls

  SELECT Nvl((SELECT Class FROM cls WHERE Col = 1), 'N/A') As ColName1
    , Nvl((SELECT Class FROM cls WHERE Col = 2), 'N/A') As ColName2
    , Nvl((SELECT Class FROM cls WHERE Col = 3), 'N/A') As ColName3
    , Nvl((SELECT Class FROM cls WHERE Col = 4), 'N/A') As ColName4
    , Nvl((SELECT Class FROM cls WHERE Col = 5), 'N/A') As ColName5
    , Nvl((SELECT Class FROM cls WHERE Col = 6), 'N/A') As ColName6
    , Nvl((SELECT Class FROM cls WHERE Col = 7), 'N/A') As ColName7

    , Nvl((SELECT SUM(Nvl(Sales, 0)) FROM salesLY WHERE Col = 1), 0) As SalesLY1
    , Nvl((SELECT SUM(Nvl(Sales, 0)) FROM salesLY WHERE Col = 2), 0) As SalesLY2
    , Nvl((SELECT SUM(Nvl(Sales, 0)) FROM salesLY WHERE Col = 3), 0) As SalesLY3
    , Nvl((SELECT SUM(Nvl(Sales, 0)) FROM salesLY WHERE Col = 4), 0) As SalesLY4
    , Nvl((SELECT SUM(Nvl(Sales, 0)) FROM salesLY WHERE Col = 5), 0) As SalesLY5
    , Nvl((SELECT SUM(Nvl(Sales, 0)) FROM salesLY WHERE Col = 6), 0) As SalesLY6
    , Nvl((SELECT SUM(Nvl(Sales, 0)) FROM salesLY WHERE Col = 7), 0) As SalesLY7
  
    , Nvl((SELECT Nvl(Sales, 0) FROM salesCY WHERE Col = 1), 0) As SalesCY1
    , Nvl((SELECT Nvl(Sales, 0) FROM salesCY WHERE Col = 2), 0) As SalesCY2
    , Nvl((SELECT Nvl(Sales, 0) FROM salesCY WHERE Col = 3), 0) As SalesCY3
    , Nvl((SELECT Nvl(Sales, 0) FROM salesCY WHERE Col = 4), 0) As SalesCY4
    , Nvl((SELECT Nvl(Sales, 0) FROM salesCY WHERE Col = 5), 0) As SalesCY5
    , Nvl((SELECT Nvl(Sales, 0) FROM salesCY WHERE Col = 6), 0) As SalesCY6
    , Nvl((SELECT Nvl(Sales, 0) FROM salesCY WHERE Col = 7), 0) As SalesCY7

    , Nvl((SELECT Nvl(Budget, 0) FROM salesCY WHERE Col = 1), 0) As BudgetCY1
    , Nvl((SELECT Nvl(Budget, 0) FROM salesCY WHERE Col = 2), 0) As BudgetCY2
    , Nvl((SELECT Nvl(Budget, 0) FROM salesCY WHERE Col = 3), 0) As BudgetCY3
    , Nvl((SELECT Nvl(Budget, 0) FROM salesCY WHERE Col = 4), 0) As BudgetCY4
    , Nvl((SELECT Nvl(Budget, 0) FROM salesCY WHERE Col = 5), 0) As BudgetCY5
    , Nvl((SELECT Nvl(Budget, 0) FROM salesCY WHERE Col = 6), 0) As BudgetCY6
    , Nvl((SELECT Nvl(Budget, 0) FROM salesCY WHERE Col = 7), 0) As BudgetCY7

  FROM dual
) INNER JOIN (  
        SELECT CY_year AS year, CY_startweek AS startweek, CY_endweek as endweek
        FROM weeks w
  ) weekInfo ON 1 = 1